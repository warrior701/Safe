/* Version: 3.1.0 */
/* (c) 2018 Skyogo Studio */
/* Released under the Apache License Version 2.0 */

window.isDebug = false;
window.pluginArr = new Array();
window.safeNameArr = new Array();
window.safeComponentArr = new Array();
console.log("Safe.js ©Skyogo工作室版权所有");
//Safe.js版本
function safeVersion(){
    return "3.1.0";
}
//Safe.js配置
function safeConfig(thing){
    for(var i in thing){
        if(i=="debug"){
            isDebug = thing[i];
        }
    }
}
//Safe.js初始化
function safeInit(thing,isReactive){
    if(isDebug){
        console.time("Safe执行时间：");
    }
    if(pluginArr.length > 0){
        for(var i=0;i<pluginArr.length;i++){
            if(i%2 != 0){
                pluginArr[i](thing,isReactive);
            }
        }
    }
    if(typeof thing.el=="string"){
        var safeThingElArr = thing.el.toString().split("&");
        for(var u = 0;u<safeThingElArr.length;u++){
            thing.el = safeThingElArr[u];
            var safeElArr = document.querySelectorAll(thing.el);
            if(thing.copy!=null&&thing.copy!=undefined){
                if(isReactive){
                    Object.defineProperty(this,"copy",{
                        set: function(newVal){
                            thing.copy = newVal;
                            document.querySelector(thing.el).innerHTML = document.querySelector(thing.copy).innerHTML;
                        }
                    })
                }
                document.querySelector(thing.el).innerHTML = document.querySelector(thing.copy).innerHTML;
            }
            if(thing.var!=null&&thing.var!=undefined){
                if(isReactive){
                    Object.defineProperty(this,"var",{
                        set: function(newVal){
                            thing.var = newVal;
                            for(var a = 0;a<safeElArr.length;a++){
                                safeElArr[a].innerHTML = safeChange(safeElArr[a].innerHTML,thing.var).do();
                            }
                        }
                    })
                }
                for(var a = 0;a<safeElArr.length;a++){
                    safeElArr[a].innerHTML = safeChange(safeElArr[a].innerHTML,thing.var).do();
                }
            }
            if(thing.css!=null&&thing.css!=undefined){
                if(isReactive){
                    Object.defineProperty(this,"css",{
                        set: function(newVal){
                            thing.css = newVal;
                            for(var a = 0;a<safeElArr.length;a++){
                                for(var i in thing.css){
                                    safeElArr[a].style[i] = thing.css[i];
                                }
                            }
                        }
                    })
                }
                for(var a = 0;a<safeElArr.length;a++){
                    for(var i in thing.css){
                        safeElArr[a].style[i] = thing.css[i];
                    }
                }
            }
            if(thing.display!=null&&thing.display!=undefined){
                if(isReactive){
                    Object.defineProperty(this,"display",{
                        set: function(newVal){
                            thing.display = newVal;
                            for(var a = 0;a<safeElArr.length;a++){
                                safeElArr[a].style.display = thing.display;
                            }
                        }
                    })
                }
                for(var a = 0;a<safeElArr.length;a++){
                    safeElArr[a].style.display = thing.display;
                }
            }
            if(thing.import!=null&&thing.import!=undefined){
                var xmlhttp;
                if (window.XMLHttpRequest){
                    xmlhttp=new XMLHttpRequest();
                }else{
                    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
                }
                xmlhttp.onreadystatechange=function(){
                    if (xmlhttp.readyState==4 && xmlhttp.status==200){
                        document.querySelector(thing.el).innerHTML = xmlhttp.responseText;
                    }
                }
                xmlhttp.open('POST',thing.import,true);
                xmlhttp.send();
            }
            if(thing.attr!=null&&thing.attr!=undefined){
                if(isReactive){
                    Object.defineProperty(this,"attr",{
                        set: function(newVal){
                            thing.attr = newVal;
                            for(var a = 0;a<safeElArr.length;a++){
                                for(var i in thing.attr){
                                    safeElArr[a].setAttribute(i,thing.attr[i])
                                }
                            }
                        }
                    })
                }
                for(var a = 0;a<safeElArr.length;a++){
                    for(var i in thing.attr){
                        safeElArr[a].setAttribute(i,thing.attr[i])
                    }
                }
            }
            if(thing.click!=null&&thing.click!=undefined){
                if(isReactive){
                    Object.defineProperty(this,"click",{
                        set: function(newVal){
                            thing.click = newVal;
                            for(var a = 0;a<safeElArr.length;a++){
                                if(typeof thing.click != "function"){
                                    for(var i in thing.method){
                                        if("##"+i+"##" == thing.click){
                                            safeElArr[a].onclick = thing.method[i];
                                        }
                                    }
                                }else{
                                    safeElArr[a].onclick = thing.click;
                                }
                            }
                        }
                    })
                }
                for(var a = 0;a<safeElArr.length;a++){
                    if(typeof thing.click != "function"){
                        for(var i in thing.method){
                            if("##"+i+"##" == thing.click){
                                safeElArr[a].onclick = thing.method[i];
                            }
                        }
                    }else{
                        safeElArr[a].onclick = thing.click;
                    }
                }
            }
            if(thing.mousemove!=null&&thing.mousemove!=undefined){
                if(isReactive){
                    Object.defineProperty(this,"mousemove",{
                        set: function(newVal){
                            thing.mousemove = newVal;
                            for(var a = 0;a<safeElArr.length;a++){
                                if(typeof thing.mousemove != "function"){
                                    for(var i in thing.method){
                                        if("##"+i+"##" == thing.mousemove){
                                            safeElArr[a].onmousemove = thing.method[i];
                                        }
                                    }
                                }else{
                                    safeElArr[a].onmousemove = thing.mousemove;
                                }
                            }
                        }
                    })
                }
                for(var a = 0;a<safeElArr.length;a++){
                    if(typeof thing.mousemove != "function"){
                        for(var i in thing.method){
                            if("##"+i+"##" == thing.mousemove){
                                safeElArr[a].onmousemove = thing.method[i];
                            }
                        }
                    }else{
                        safeElArr[a].onmousemove = thing.mousemove;
                    }
                }
            }
            if(thing.mousedown!=null&&thing.mousedown!=undefined){
                if(isReactive){
                    Object.defineProperty(this,"mousedown",{
                        set: function(newVal){
                            thing.mousedown = newVal;
                            for(var a = 0;a<safeElArr.length;a++){
                                if(typeof thing.mousedown != "function"){
                                    for(var i in thing.method){
                                        if("##"+i+"##" == thing.mousedown){
                                            safeElArr[a].onmousedown = thing.method[i];
                                        }
                                    }
                                }else{
                                    safeElArr[a].onmousedown = thing.mousedown;
                                }
                            }
                        }
                    })
                }
                for(var a = 0;a<safeElArr.length;a++){
                    if(typeof thing.mousedown != "function"){
                        for(var i in thing.method){
                            if("##"+i+"##" == thing.mousedown){
                                safeElArr[a].onmousedown = thing.method[i];
                            }
                        }
                    }else{
                        safeElArr[a].onmousedown = thing.mousedown;
                    }
                }
            }
            if(thing.mouseover!=null&&thing.mouseover!=undefined){
                if(isReactive){
                    Object.defineProperty(this,"mouseover",{
                        set: function(newVal){
                            thing.mouseover = newVal;
                            for(var a = 0;a<safeElArr.length;a++){
                                if(typeof thing.mouseover != "function"){
                                    for(var i in thing.method){
                                        if("##"+i+"##" == thing.mouseover){
                                            safeElArr[a].onmouseover = thing.method[i];
                                        }
                                    }
                                }else{
                                    safeElArr[a].onmouseover = thing.mouseover;
                                }
                            }
                        }
                    })
                }
                for(var a = 0;a<safeElArr.length;a++){
                    if(typeof thing.mouseover != "function"){
                        for(var i in thing.method){
                            if("##"+i+"##" == thing.mouseover){
                                safeElArr[a].onmouseover = thing.method[i];
                            }
                        }
                    }else{
                        safeElArr[a].onmouseover = thing.mouseover;
                    }
                }
            }
            if(thing.mouseout!=null&&thing.mouseout!=undefined){
                if(isReactive){
                    Object.defineProperty(this,"mouseout",{
                        set: function(newVal){
                            thing.mouseout = newVal;
                            for(var a = 0;a<safeElArr.length;a++){
                                if(typeof thing.mouseout != "function"){
                                    for(var i in thing.method){
                                        if("##"+i+"##" == thing.mouseout){
                                            safeElArr[a].onmouseout = thing.method[i];
                                        }
                                    }
                                }else{
                                    safeElArr[a].onmouseout = thing.mouseout;
                                }
                            }
                        }
                    })
                }
                for(var a = 0;a<safeElArr.length;a++){
                    if(typeof thing.mouseout != "function"){
                        for(var i in thing.method){
                            if("##"+i+"##" == thing.mouseout){
                                safeElArr[a].onmouseout = thing.method[i];
                            }
                        }
                    }else{
                        safeElArr[a].onmouseout = thing.mouseout;
                    }
                }
            }
            if(thing.change!=null&&thing.change!=undefined){
                if(isReactive){
                    Object.defineProperty(this,"change",{
                        set: function(newVal){
                            thing.change = newVal;
                            for(var a = 0;a<safeElArr.length;a++){
                                if(typeof thing.change != "function"){
                                    for(var i in thing.method){
                                        if("##"+i+"##" == thing.change){
                                            safeElArr[a].onchange = thing.method[i];
                                        }
                                    }
                                }else{
                                    safeElArr[a].onchange = thing.change;
                                }
                            }
                        }
                    })
                }
                for(var a = 0;a<safeElArr.length;a++){
                    if(typeof thing.change != "function"){
                        for(var i in thing.method){
                            if("##"+i+"##" == thing.change){
                                safeElArr[a].onchange = thing.method[i];
                            }
                        }
                    }else{
                        safeElArr[a].onchange = thing.change;
                    }
                }
            }
            if(thing.html!=null&&thing.html!=undefined){
                if(isReactive){
                    Object.defineProperty(this,"html",{
                        set: function(newVal){
                            thing.html = newVal;
                            for(var a = 0;a<safeElArr.length;a++){
                                safeElArr[a].innerHTML = thing.html;
                            }
                        }
                    })
                }
                for(var a = 0;a<safeElArr.length;a++){
                    safeElArr[a].innerHTML = thing.html;
                }
            }
            if(thing.text!=null&&thing.text!=undefined){
                if(isReactive){
                    Object.defineProperty(this,"text",{
                        set: function(newVal){
                            thing.text = newVal;
                            for(var a = 0;a<safeElArr.length;a++){
                                safeElArr[a].innerText = thing.text;
                            }
                        }
                    })
                }
                for(var a = 0;a<safeElArr.length;a++){
                    safeElArr[a].innerText = thing.text;
                }
            }
            if(thing.class!=null&&thing.class!=undefined){
                if(isReactive){
                    Object.defineProperty(this,"class",{
                        set: function(newVal){
                            thing.class = newVal;
                            for(var a = 0;a<safeElArr.length;a++){
                                safeElArr[a].setAttribute("class",thing.class);
                            }
                        }
                    })
                }
                for(var a = 0;a<safeElArr.length;a++){
                        safeElArr[a].setAttribute("class",thing.class);
                }
            }
            if(thing.name!=null&&thing.name!=undefined){
                if(isReactive){
                    Object.defineProperty(this,"name",{
                        set: function(newVal){
                            thing.name = newVal;
                            for(var a = 0;a<safeElArr.length;a++){
                                safeElArr[a].setAttribute("name",thing.name);
                            }
                        }
                    })
                }
                for(var a = 0;a<safeElArr.length;a++){
                    safeElArr[a].setAttribute("name",thing.name);
                }
            }
            if(thing.bindVal!=null&&thing.bindVal!=undefined){
                if(isReactive){
                    Object.defineProperty(this,"bindVal",{
                        set: function(newVal){
                            thing.bindVal = newVal;
                            var safeBindTargetElArr = document.querySelectorAll(thing.bindVal);
                            for(var a = 0;a<safeElArr.length;a++){
                                eval("safeElArr[a].oninput = function(){for(var u=0;u<safeBindTargetElArr.length;u++){safeBindTargetElArr[u].value = safeElArr["+a+"].value}}");
                            }
                        }
                    })
                }
                var safeBindTargetElArr = document.querySelectorAll(thing.bindVal);
                for(var a = 0;a<safeElArr.length;a++){
                    eval("safeElArr[a].oninput = function(){for(var u=0;u<safeBindTargetElArr.length;u++){safeBindTargetElArr[u].value = safeElArr["+a+"].value}}");
                }
            }
            if(thing.bindHTML!=null&&thing.bindHTML!=undefined){
                if(isReactive){
                    Object.defineProperty(this,"bindHTML",{
                        set: function(newVal){
                            thing.bindHTML = newVal;
                            var safeBindTargetElArr = document.querySelectorAll(thing.bindHTML);
                            for(var a = 0;a<safeElArr.length;a++){
                                eval("safeElArr[a].oninput = function(){for(var u=0;u<safeBindTargetElArr.length;u++){safeBindTargetElArr[u].innerHTML = safeElArr["+a+"].value}}");
                            }
                        }
                    })
                }
                var safeBindTargetElArr = document.querySelectorAll(thing.bindHTML);
                for(var a = 0;a<safeElArr.length;a++){
                    eval("safeElArr[a].oninput = function(){for(var u=0;u<safeBindTargetElArr.length;u++){safeBindTargetElArr[u].innerHTML = safeElArr["+a+"].value}}");
                }
            }
            if(thing.bindText!=null&&thing.bindText!=undefined){
                if(isReactive){
                    Object.defineProperty(this,"bindText",{
                        set: function(newVal){
                            thing.bindText = newVal;
                            var safeBindTargetElArr = document.querySelectorAll(thing.bindText);
                            for(var a = 0;a<safeElArr.length;a++){
                                eval("safeElArr[a].oninput = function(){for(var u=0;u<safeBindTargetElArr.length;u++){safeBindTargetElArr[u].innerText = safeElArr["+a+"].value}}");
                            }
                        }
                    })
                }
                var safeBindTargetElArr = document.querySelectorAll(thing.bindText);
                for(var a = 0;a<safeElArr.length;a++){
                    eval("safeElArr[a].oninput = function(){for(var u=0;u<safeBindTargetElArr.length;u++){safeBindTargetElArr[u].innerText = safeElArr["+a+"].value}}");
                }
            }
        /* 需要thing.el的内容在这个大括号前面写 */    
    }
    }
    /* CallBack永远放到最后 */
    if(thing.callback!=null&&thing.callback!=undefined){
        if(typeof thing.callback != "function"){
            for(var i in thing.method){
                if("##"+i+"##" == thing.callback){
                    thing.method[i]();
                }
            }
        }else{
            thing.callback();
        }
    }
    if(isDebug){
        console.timeEnd("Safe执行时间：");
    }
}
//Safe.js模板方法
function safeChange(content,varList){
    var safeChangeDivisionSign = "";
    this.content=content;
    this.varList=varList;
    this.do=function(){
        for(var i in this.varList){
            if(i == "DivisionSign"){
                safeChangeDivisionSign = this.varList[i];
            }
            try{
                var safeChangeString = this.varList[i].join(safeChangeDivisionSign);
                this.content=this.content.replace(new RegExp("-##"+i+"-##","g"),safeChangeString);
            }catch(err){}
            this.content=this.content.replace(new RegExp("##"+i+"##","g"),this.varList[i].toString());
            try{
                var safeChangeString = this.varList[i].join(safeChangeDivisionSign);
                this.content=this.content.replace(new RegExp("-#"+i+"-#","g"),safeChangeString.replace(/&/g,"&amp;").replace(/</g,"&lt;").replace(/\'/g,"&apos;").replace(/\"/g,"&quot;").replace(/>/g,"&gt;"));
            }catch(err){}
            this.content=this.content.replace(new RegExp("#"+i+"#","g"),this.varList[i].toString().replace(/&/g,"&amp;").replace(/</g,"&lt;").replace(/\'/g,"&apos;").replace(/\"/g,"&quot;").replace(/>/g,"&gt;"));
        }
        return this.content;
    }
    if(this==window){
        return new safeChange(content,varList);
    }
}
//Safe.js创建组件方法
function safeComponent(name,html){
    if(name!=null&&name!=undefined){
        for(var a = 0;a<safeComponentArr.length;a++){
            if(safeComponentArr[a]==name,a%2 == 0){
                if(html!=null&&html!=undefined){
                    safeComponentArr[a+1] = html;
                }
                var safeComponentElArr = document.getElementsByTagName(name);
                for(var i=0;i<safeComponentElArr.length;i++){
                    safeComponentElArr[i].innerHTML = safeComponentArr[a+1];
                }
                return;
            }
        }
        var safeComponentElArr = document.getElementsByTagName(name);
        safeComponentArr[safeComponentArr.length] = name;
        safeComponentArr[safeComponentArr.length] = html;
        for(var i=0;i<safeComponentElArr.length;i++){
            safeComponentElArr[i].innerHTML = html;
        }
    }else{
        if(isDebug){
            console.error("Safe: name is null.");
        }
    }
}
//获取所有插件方法
function safeGetAllPlugin(){
    var safePluginNameArr = new Array();
    for(var i=0;i<pluginArr.length;i++){
        if(i%2 == 0){
            safePluginNameArr[safePluginNameArr.length] = pluginArr;
        }
    }
    return safePluginNameArr;
}
//插件传递方法
function safePlugin(name,content){
    pluginArr[pluginArr.length] = name;
    pluginArr[pluginArr.length] = content;
}
//safe输入方法
function safeIn(el){
    this.el = el;
    this.html = function(){//获取html
        var val = "";
        for(var i = 0;i<safeInEl.length;i++){
            val += safeInEl[i].innerHTML;
        }
        return val;
    }
    this.text = function(){//获取text
        var val = "";
        for(var i = 0;i<safeInEl.length;i++){
            val += safeInEl[i].innerText;
        }
        return val;
    }
    this.val = function(){//获取value
        var val = "";
        for(var i = 0;i<safeInEl.length;i++){
            val += safeInEl[i].value;
        }
        return val;
    }
    this.attr = function(name){//获取属性
        var val = "";
        for(var i = 0;i<safeInEl.length;i++){
            val += safeInEl[i].getAttribute(name);
        }
        return val;
    }
    this.css = function(name){//获取css
        var val = "";
        for(var i = 0;i<safeInEl.length;i++){
            val += safeInEl[i].style[name];
        }
        return val;
    }
    this.height = function(){//获取height
        var val = "";
        for(var i = 0;i<safeInEl.length;i++){
            val += safeInEl[i].getAttribute("height");
        }
        return val;
    }
    this.width = function(){//获取width
        var val = "";
        for(var i = 0;i<safeInEl.length;i++){
            val += safeInEl[i].getAttribute("width");
        }
        return val;
    }
    var safeInEl;
    if(this.el!=undefined&&this.el!=null&&this.el!=""){
        safeInEl = document.querySelectorAll(this.el);
    }else{
        return;
    }
    if(this==window){
        return new safeIn(el);
    }
}
//safeIn替身
function _(el){
    if(this==window){
        return new safeIn(el);
    }
}